﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace EigenHwapp
{
    class LearnCSharp
    {
        public static void CountTo(int to)
        {
            int number = 1;

            while (number <= to)
            {
                Console.WriteLine(number);
                number = number + 1;
            }
        }

        public static void ShowMMORPG()
        {
            ArrayList list = new ArrayList();
            list.Add("FFXIV");
            list.Add("WOW");
            list.Add("GW2");

            foreach (string name in list)
                Console.WriteLine(name);

        }
    }
}
